# Official Repository of the Computational Psychiatry Course 2020 #

Welcome to the CPC2020 Repository

### Contact ###
Dr. Frederike Petzschner

petzschner@biomed.ee.ethz.ch


### What is this repository for? ###

This Repo provides you with

- the Slides of the (virtual) Computational Psychiatry Course in Zurich as pdf
- additional Code presented during talks or tutorials

